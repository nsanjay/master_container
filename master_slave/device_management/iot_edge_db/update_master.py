#########################################################################################
# This program facilitates database operations for master device
#########################################################################################

import json
import pymongo

my_client = pymongo.MongoClient("mongodb://mongodb:27017/")
db = my_client["Master"]


def get_configuration():
    posts = db.MQTT_Topics

    for connection_list in posts.find().sort("_id", -1):
        azure_string = connection_list['Azure_String']
        master_topic = connection_list['master_topic']
        master_topic1 = connection_list['master_topic1']
        slave_topic = connection_list['slave_topic']
        iot_topic = connection_list['iot_topic']
        break

    return azure_string, master_topic, master_topic1, slave_topic, iot_topic


def update_device_database(device_name, priority, device_type, food_camera, face_camera):
    name = []
    posts = db.Slaves_Configuration

    for connection_list in posts.find():
        name.append(connection_list["Name"])

    if device_name not in name:
        data_str = {"Name": device_name, "Priority": priority, "Type": device_type, "Food_Camera": food_camera,
                    "Face_Camera": face_camera}
        posts.insert_one(data_str)
    elif device_name in name:
        filter1 = {"Name": device_name}
        new_val = {"$set": {"Priority": priority, "Type": device_type, "Food_Camera": food_camera,
                            "Face_Camera": face_camera}}
        posts.update_many(filter1, new_val)


def get_slave_device():
    name = []
    posts = db.Slaves_Configuration

    for connection_list in posts.find():
        name.append(connection_list["Name"])

    return name


def get_device_information():
    name = []
    priority = []
    food_camera = []
    face_camera = []

    posts = db.Slaves_Configuration

    for connection_list in posts.find():
        name.append(connection_list["Name"])
        priority.append(connection_list["Priority"])
        food_camera.append(connection_list["Food_Camera"])
        face_camera.append(connection_list["Face_Camera"])

    return name, priority, food_camera, face_camera
