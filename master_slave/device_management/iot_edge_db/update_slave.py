#########################################################################################
# This program facilitates database operations for slave device
#########################################################################################

import json
import pymongo

my_client = pymongo.MongoClient("mongodb://mongodb:27017/")
db = my_client["Slave"]


def get_device_configuration():
    # edge_station = ""
    # priority = ""
    # device_type = ""
    # food_camera = ""
    # face_camera = ""
    posts = db.Configuration
    list1 = posts.find()
    for item in list1:
        edge_station = item["Name"]
        priority = item["Priority"]
        device_type = item["Type"]
        food_camera = "Food_Camera"
        face_camera = "Food_Camera"
    return edge_station, priority, device_type, food_camera, face_camera


def update_device_database(device_name, priority, food_camera, face_camera):
    name = []
    posts = db.Slaves_Configuration

    for connection_list in posts.find():
        name.append(connection_list["Name"])
    
    for i in range(len(device_name)):
        if device_name[i] not in name:
            data_str = {"Name": device_name[i], "Priority": priority[i], "Food_Camera": food_camera[i],
                        "Face_Camera": face_camera[i]}
            posts.insert_one(data_str)
        elif device_name[i] in name:
            filter1 = {"Name": device_name[i]}
            new_val = {"$set": {"Priority": priority[i], "Food_Camera": food_camera[i], "Face_Camera": face_camera[i]}}
            posts.update_many(filter1, new_val)


def get_device_priority():
    device_name = []
    device_priority = []
    
    posts=db.Slaves_Configuration

    for connection_list in posts.find():
        device_name.append(connection_list["Name"])
        device_priority.append(connection_list["Priority"])

    return device_name,device_priority


def get_configuration():
    posts = db.MQTT_Topics

    for connection_list in posts.find().sort("_id", -1):
        azure_string = connection_list['Azure_String']
        master_topic = connection_list['master_topic']
        master_topic1 = connection_list['master_topic1']
        slave_topic = connection_list['slave_topic']
        iot_topic = connection_list['iot_topic']
        break

    return azure_string, master_topic, master_topic1, slave_topic, iot_topic