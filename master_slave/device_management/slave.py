import os
import pymongo
import paho.mqtt.publish as publish
import paho.mqtt.subscribe as subscribe
import psutil
from threading import Thread
import time
import json

from config import config
from get_device_records import get_record


def new_master():
    global master_status
    web_online = 0

    while True:
        true_offline = []
        if master_status == 'Online':
            pass 
            # if web_online == 1:
                # update.update_slave_status_to_master(slave_Device_Name,'slave')
                # web_online = 0
        else:
            # web_online = 1
            # update.update_slave_status_to_master(slave_Device_Name,'master')

            offline_device = []
            online_device = []
            device_cpu = []
            get_record.update_master_status()

            data = {"Message":"Update_Status"}
            data = json.dumps(data)
            publish.single("master_topic", data, hostname="mqtt.eclipse.org")
            time.sleep(5)
            publish.single("master_topic", data, hostname="mqtt.eclipse.org")
            time.sleep(5)

            name,cpu,status = get_record.get_database_record()
            # print(name,cpu,status)

            for i in range(len(status)):
                if status[i] == "offline":
                    offline_device.append(name[i])
                else:
                    online_device.append(name[i])
                    device_cpu.append(cpu[i])

            # update_frontend(offline_device,online_device,device_cpu)
            os.system("echo Offline: " + str(offline_device) + " Online: " + str(online_device) + " CPU: " + str(device_cpu))

            for i in range(len(device_cpu)):
                if device_cpu[i] > 90:
                    os.system("echo " + "Maximum CPU Utilization Edge Station: " + str(online_device[i]))
            

def send_status():
    while True:
        data = {"Message":slave_Device_Name}
        data = json.dumps(data)
        publish.single("slave_topic", data, hostname="mqtt.eclipse.org")
        time.sleep(send_time)

def check_master():
    global master_status
    start_time = time.time()

    while True:
        msg = subscribe.simple("slave_topic", hostname="mqtt.eclipse.org")
        data = msg.payload.decode("UTF-8")
        data = json.loads(data)
        
        if (data['Message']) in listener:#'Master':
            master_status = 'Online'
            time.sleep(2)
            print(data)
            start_time = time.time()

        if (time.time() - start_time) > check_time:
            time.sleep(2)
            print("Master Offline")
            master_status = 'Offline'

def update_slave_status():
    while True:
        msg = subscribe.simple("master_topic", hostname="mqtt.eclipse.org")
        data = msg.payload.decode("UTF-8")
        data = json.loads(data)

        if data['Message'] == "Update_Status":
            # print("Update Database")
            get_record.update_device_status(slave_Device_Name)
        time.sleep(1)


os.system("echo 'Starting slave MQTT communication...'")
master_status = 'Online'
slave_Device_Name = config.slave_name #"Edge-station-1"
get_record.create_db(slave_Device_Name)

pri_device,priority_data = get_record.getpriority()
sorted_priority = [pri_device for _,pri_device in sorted(zip(priority_data,pri_device))]
priority_data.sort()
print(sorted_priority,priority_data)
index = sorted_priority.index(slave_Device_Name)
priority = priority_data[index]

listener = sorted_priority[0:index]
print(listener)

if priority == '1':
    send_time = 3
    check_time = 15
elif priority == '2':
    send_time = 5
    check_time = 30
else:
    send_time = 5
    check_time = 45


thread = Thread(target=send_status)
thread.start()
thread1 = Thread(target=update_slave_status)
thread1.start()
thread2 = Thread(target=check_master)
thread2.start()
thread3 = Thread(target = new_master)
thread3.start()

while True:
    if not thread.is_alive():
        thread = Thread(target = send_status)
        thread.start()

    if not thread1.is_alive():
        thread1 = Thread(target = update_slave_status)
        thread1.start()

    if not thread2.is_alive():
        thread2 = Thread(target = check_master)
        thread2.start()

    if not thread3.is_alive():
        thread3 = Thread(target = new_master)
        thread3.start()