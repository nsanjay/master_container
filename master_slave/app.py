#!/usr/bin/env python3

#######################################################################################
# This program kicks off appropriate program based on the type of the registered device
# in the locally hosted database.

# Usage : python3 app.py
#######################################################################################

import subprocess as sp
import time

from device_management.iot_edge_db import update_slave

# Unused variables will be utilised in upcoming updates
time.sleep(20)

if __name__ == "__main__":
    extProc = sp.Popen(['python3', 'device_management/master.py'])
